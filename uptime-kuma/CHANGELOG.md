## What’s changed

## ⬆️ Dependency updates

- ⬆️ Upgrades Uptime Kuma to 1.21.0 @frenck ([#60](https://github.com/hassio-addons/addon-uptime-kuma/pull/60))

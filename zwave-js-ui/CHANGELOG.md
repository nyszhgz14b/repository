## What’s changed

## ⬆️ Dependency updates

- ⬆️ Upgrades Z-Wave JS UI to v8.11.0 @frenck ([#483](https://github.com/hassio-addons/addon-zwave-js-ui/pull/483))
